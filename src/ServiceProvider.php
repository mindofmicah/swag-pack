<?php
namespace MindOfMicah\SwagPack;

use Illuminate\Support\ServiceProvider as SP;

class ServiceProvider extends SP
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config.php', 'swag-pack');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes.php');
        $this->publishes([
            __DIR__ . '/../config.php' => config_path('swag-pack.php'),
        ]);
        if ($this->app->runningInConsole()) {
            $this->commands(
                [
                    InstallSwaggerUICommand::class,
                ]
            );
        }
    }
}
