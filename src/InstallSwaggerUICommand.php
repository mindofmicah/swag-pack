<?php
namespace MindOfMicah\SwagPack;

use Illuminate\Console\Command;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Filesystem\Filesystem;

class InstallSwaggerUICommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'swag-pack:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Filesystem $filesystem, ConfigRepository $config)
    {
        // Copy all HTML
        $asset_directory = public_path($config->get('swag-pack.swagger-ui.dest'));
        $index_file = $config->get('swag-pack.cached-index');

        $filesystem->copyDirectory(base_path($config->get('swag-pack.swagger-ui.src')), $asset_directory);

        $this->transformAssetFile(
            $filesystem,
            $asset_directory . '/' . $index_file,
            fn(string $contents) => str_replace(
                '"./swagger',
                '"/' . $config->get('swag-pack.swagger-ui.dest') . '/swagger',
                $contents
            ),
            storage_path($index_file)
        );

        $this->transformAssetFile(
            $filesystem,
            public_path('swag-pack/swagger-initializer.js'),
            fn($contents) => str_replace(
                'url: "https://petstore.swagger.io/v2/swagger.json"',
                'url: "' . $config->get('swag-pack.paths.config_route') . '"',
                $contents
            ),
        );
    }

    private function transformAssetFile(
        Filesystem $filesystem,
        string $filename,
        callable $transformer,
        string $dest = null
    ) {
        $contents = $filesystem->get($filename);
        $filesystem->put($dest ?? $filename, $transformer($contents));
        if ($dest) {
            $filesystem->delete($filename);
        }
    }
}
