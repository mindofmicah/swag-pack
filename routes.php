<?php
Route::group([], function () {
Route::get('docs',
        function () {

            $index_file = config()->get('swag-pack.cached-index');

            return file_get_contents(storage_path($index_file));
        }
    )->middleware('auth');
});
