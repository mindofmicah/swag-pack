<?php
return [
    'swagger-ui' => [
        'src'  => '/vendor/swagger-api/swagger-ui/dist',
        'dest' => 'swag-pack',
    ],
    'cached-index'=>'index.html',
    'paths'      => [
        'config_route' => '/swagger.json',
    ],
];
